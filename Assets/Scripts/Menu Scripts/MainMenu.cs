﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{

    [SerializeField] LevelChanger levelChanger;


    public void PlayGame()
    {
        //FindObjectOfType<AudioManager>().Play("MenuClick1");
        levelChanger.FadeToLevel(1);
    }

    public void QuitGame()
    {
        UnityEditor.EditorApplication.isPlaying = false; // A enlever pour le build
        //FindObjectOfType<AudioManager>().Play("MenuClick2");
        Application.Quit();
    }

    public void Options()
    {
        FindObjectOfType<AudioManager>().Play("Ouvrir_journal");  
    }

    public void Back()
    {
        FindObjectOfType<AudioManager>().Play("Ouvrir_journal");  
    }

}